// The "fetch" method will return a "promise" that resolves to a "response" object
fetch('https://jsonplaceholder.typicode.com/posts')
// The first ".then" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
// The second ".then" method prints the converted JSON valuse from the "fetch" request
.then((data) => showPosts(data));

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	// Prevents the page from reloading
	// prevents default behavior of event
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
			// JSON.stringify converts the obect into strigified JSON
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),

		//Sets the header data of the "request" object to be sent to be sent to the backend
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added!');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});

});


// RETRIEVE POSTS
const showPosts = (posts) => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id ="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});

	//console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;

};


// EDIT POST

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	// To remove the 'disabled' attribute from the update button
	// removeAttribute() - removes the attribute with the specified name from the element
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};


// UPDATE POST
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),
		//Sets the header data of the "request" object to be sent to be sent to the backend
		headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully updated!');

	// Resets the edit post
	document.querySelector('#txt-edit-id').value = null;
	document.querySelector('#txt-edit-title').value = null;
	document.querySelector('#txt-edit-body').value = null;

	// .setAttribute - sets an attribute to an HTML element
	document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
});

// ACTIVITY CODES - DELETE
const deletePost = (id) => {
	// //Loops thru all elements of the array to find the post ID that matches the post selects
	// posts = posts.filter((post) => {
	// 	if(post.id.toString() !== id){
	// 		return post;
	// 	}
	// });

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {method: 'DELETE'});
	// The Element.remove() method removes the element from the DOM.
	document.querySelector(`#post-${id}`).remove();
};